" Final configs to ensure settings.

" The character â is mapped to ALT+b (not sure if by vim or by the terminal).
" Anyway, this is a non-us keyboard issue.
" So let's 'unmmap' it.
" https://github.com/jiangmiao/auto-pairs/issues/88
let g:AutoPairsShortcutBackInsert=''
set keymap=accents
