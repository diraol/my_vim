"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Maintainer:
"       Diego Rabatone Oliveira - @diraol
"
" Reference:
" 	This vim configuration is based on:
" 	https://github.com/amix/vimrc/
"
" Sections:
"    -> General
"    -> VIM user interface
"    -> Colors and Fonts
"    -> Files and backups
"    -> Text, tab and indent related
"    -> Visual mode related
"    -> Moving around, tabs and buffers
"    -> Status line
"    -> Editing mappings
"    -> vimgrep searching and cope displaying
"    -> Spell checking
"    -> Misc
"    -> Helper functions
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use Vim defaults instead of 100% vi compatibility
set nocompatible

set runtimepath+=~/.diraol/vim

" Install plug is not installed and install uninstalled plygins
if empty(glob('~/.diraol/vim/autoload/plug.vim'))
	silent !curl -fLo ~/.diraol/vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin("~/.diraol/vim/my_plugins")
  Plug 'morhetz/gruvbox'
call plug#end()

source ~/.diraol/vim/myplugins.vim
source ~/.diraol/vim/basic.vim
source ~/.diraol/vim/filetypes.vim
source ~/.diraol/vim/plugins_config.vim
source ~/.diraol/vim/extended.vim
source ~/.diraol/vim/final.vim
