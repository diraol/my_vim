" Here we just list the plugins we want installed.

call plug#begin("~/.diraol/vim/my_plugins")
  " IDE related plugins
  Plug 'morhetz/gruvbox'
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'christoomey/vim-tmux-navigator'
  Plug 'ryanoasis/vim-devicons'
  Plug 'vim-ctrlspace/vim-ctrlspace'
  Plug 'vim-syntastic/syntastic'
  Plug 'tpope/vim-surround'
  Plug 'preservim/nerdtree'
  Plug 'Xuyuanp/nerdtree-git-plugin'
  Plug 'preservim/nerdcommenter'
  Plug 'airblade/vim-gitgutter'
  Plug 'tpope/vim-fugitive'
  Plug 'jiangmiao/auto-pairs'
  Plug 'ycm-core/YouCompleteMe', { 'do': './install.py --go-completer' }
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install --all' }
  Plug 'majutsushi/tagbar'
  Plug 'terryma/vim-multiple-cursors'
  Plug 'junegunn/goyo.vim'
  Plug 'amix/vim-zenroom2'
  " Plug 'AlphaMycelium/pathfinder.vim'
  Plug 'Konfekt/FastFold'
  Plug 'tmhedberg/SimpylFold'

  " Language specific plugins
  Plug 'tmux-plugins/vim-tmux'
  Plug 'jmcantrell/vim-virtualenv'
  Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
  Plug 'hashivim/vim-terraform', { 'for': ['hcl', 'tf'] }
  Plug 'hashivim/vim-packer'
  Plug 'lervag/vimtex', { 'for': 'tex' }
  Plug 'chrisbra/csv.vim', { 'for': 'csv' }
  Plug 'groenewege/vim-less', { 'for': ['css', 'less', 'scss'] }
  Plug 'skammer/vim-css-color', { 'for': ['css', 'less', 'scss'] }
  Plug 'hail2u/vim-css3-syntax', { 'for': ['css', 'less', 'scss'] }
  Plug 'pangloss/vim-javascript', { 'for': 'js' }
  Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
  Plug 'vim-scripts/nginx.vim'
  Plug 'godlygeek/tabular'
  Plug 'plasticboy/vim-markdown', { 'for': 'md' }
  Plug 'martinda/Jenkinsfile-vim-syntax'
  Plug 'derekwyatt/vim-scala'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'scalameta/coc-metals', {'do': 'yarn install --frozen-lockfile'}
  " Plug 'dense-analysis/ale'

call plug#end()

" https://github.com/junegunn/vim-plug/wiki/extra
" Automatically install missing plugins on startup
autocmd VimEnter *
	\ if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
	\|	PlugInstall --sync | q
	\| endif
